const REPLY_TYPES = {
    REPLY: 'reply',
    SKIP_REPLY: 'skipReply',
    HINT_REPLY: 'hintReply'
}

exports.REPLY_TYPES = REPLY_TYPES;