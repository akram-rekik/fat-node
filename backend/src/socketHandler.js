const aliceBot = require('./alice-bot').aliceBot;
const REPLY_TYPES = require('./reply-types').REPLY_TYPES;

/**
  * The SocketHandler class handles sockets
  */
class SocketHandler {

  /**
    * Creates a new SocketHandler
    */
  constructor() {
    // let's make any easy dialog of 3 steps in order to guide Alice fir her journey
    this.step = 0;
  }

  /**
    * Logs that a user connected
    */
  onConnect(/* istanbul ignore next */{c = console} = {}){
    c.log('a user connected');
  }

  /**
    * Logs that a user disconnected
    */
  onDisconnect(/* istanbul ignore next */{c = console} = {}){
    c.log('a user disconnected');
  }

  /**
    * Responds to a message
    */
  onMessage(/* istanbul ignore next */{msg, socket, c = console} = {}){
    c.log('message: ' + msg);
    const reply = aliceBot(msg.replace(/\s/gi,'').toLowerCase(), this.step);
    if(reply.type === REPLY_TYPES.REPLY || reply.type === REPLY_TYPES.SKIP_REPLY) {
      this.step += 1;
    }
    // restart the chat steps from the begining
    if(this.step > 3) {
      this.step = 0;
    }
    c.log('message step: ' + this.step);
    socket.emit('chat message', reply.msg);
  }

}

exports.SocketHandler = SocketHandler;
