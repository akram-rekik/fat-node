const REPLY_TYPES = require('./reply-types').REPLY_TYPES;
/**
 * reply to messages comming from Alice
 * 
 * @param {string} msg 
 * @param {number} step
 * 
 * @return { object} reply - e.g {msg : 'Hey Alice', type: 'reply'}
 */
function aliceBot(msg, step) {
    let reply;
    let hintReply;
    let skipReply;

    if(step === 0) {
        reply = `Hey Alice, I am Aldon Bot a simple chat bot
                <br />that can help make your stay at Aldon Hotel.
                <br />Do you want to know about perks/surrowndig ?`;
        hintReply = `Sorry, I do not understand this
                    <br /> could please enter "hello" to start`;
        if(msg === 'hello') {
            return { msg: reply, type: REPLY_TYPES.REPLY};
        }                   
        return { msg: hintReply, type: REPLY_TYPES.HINT_REPLY };
    }

    if(step === 1) {
        reply = `Alice, immerse yourself in the alluring metropolis of Berlin, the capital of reunified Germany.
                <br /> Discover new facets in this cultural and fashion hub and soak up the vibrant atmosphere.
                <br />Visit the follwing url for more info https://www.kempinski.com/en/berlin/hotel-adlon/local-information/
                <br />do you want to know about rooms`;
        skipReply = `Alice, do you want to know about rooms for booking in within Adlon?`
        hintReply = `Sorry, I do not understand this
                    <br /> could please answer the last question by "yes" or "no" ?`;    
        switch (msg) {
            case 'yes': return { msg: reply, type: 'reply' };
            case 'no' : return { msg: skipReply, type: 'skipReply' };
            default: return { msg: hintReply, type: 'hintReply' };
        }
        return;
    }

    if(step === 2) {
        reply = `Alice, discover upscale romms with amazing features and amenities
                <br /> Please have a look here https://www.kempinski.com/en/berlin/hotel-adlon/rooms-and-suites/
                <br />Do you want to book a room?`;
        skipReply = `Alice, do you want to book a room?`
        hintReply = `Sorry, I do not understand this
                    <br /> could please answer the last question by "yes" or "no" ?`; 
        switch (msg) {
            case 'yes': return { msg: reply, type: 'reply' };
            case 'no' : return { msg: skipReply, type: 'skipReply' };
            default: return { msg: hintReply, type: 'hintReply' };
        }
        return;
    }

    if(step === 3) {
        reply = `Alice, in order to book a room and check availibilties, visit the following url
                <br />https://www.kempinski.com/KIBER2/en/booking/booking-room-availability/
                <br />I am done ! I hope I could help through our converstaion. See you^^`;
        skipReply = 'Alice, I cannot provide further information. See you'
        hintReply = `Sorry, I do not understand this
                    <br /> could please answer the last question by "yes" or "no" ?`;    
        switch (msg) {
            case 'yes': return { msg: reply, type: 'reply' };
            case 'no' : return { msg: skipReply, type: 'skipReply' };
            default: return { msg: hintReply, type: 'hintReply' };
        }
        return;
    }
}

exports.aliceBot = aliceBot;